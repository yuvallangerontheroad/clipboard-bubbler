# Clipboard Bubbler.

Shows the latest clipboard contents in a nice little bubble.

## Dependencies:

Python GTK stuff.

## Useful links:

If you copy text from a website you will get something entirely different:

<https://www.wizer-training.com/blog/copy-paste>

A clipboard write watcher written in C:

<https://askubuntu.com/a/1167129/325213>
leads to
<https://github.com/SidMan2001/Scripts/tree/master/PDF-Copy-without-Linebreaks-Linux>
leads to 
<https://github.com/cdown/clipnotify>

A web page which breaks apart a piece of text into its codepoints:

<https://www.textmagic.com/free-tools/unicode-detector>

Clipboard on the Python GTK bindings:

<https://python-gtk-3-tutorial.readthedocs.io/en/latest/clipboard.html>

How to catch a clipboard change event (thank you, Ikaros!):

<https://stackoverflow.com/a/25961646>

Stackoverflow being stackoverflow ("This is just a rite of passage" my ass…):

<https://meta.stackoverflow.com/questions/351807/consider-displaying-zero-width-space-characters-in-code-blocks>

import typing
import pprint
import subprocess

import gi

gi.require_version("Gtk", "3.0")
# gi.require_version("AppIndicator3", "0.1")
gi.require_version('Notify', '0.7')
from gi.repository import Gtk, Gdk, Notify# , AppIndicator3


BACKWARDS_NAME = 'farm.kaka.clipboardbubbler'


class TextWindow(Gtk.Window):
    def __init__(self, clipboard_contents: str) -> None:
        super().__init__(title='Clipboard Bubbler')

        self.set_default_size(500, 100)

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_hexpand(True)
        self.scrolled_window.set_vexpand(True)

        self.text_view = Gtk.TextView()
        self.text_buffer = self.text_view.get_buffer()
        self.text_buffer.set_text(clipboard_contents)

        self.scrolled_window.add(self.text_view)

        self.add(self.scrolled_window)



def callback(
    clipboard: Gtk.Clipboard,
):
    def callback(_a, _b) -> None:
        clipboard_contents = clipboard.wait_for_text()

        notification = Notify.Notification.new('Clipboard Bubbler', clipboard_contents)
        #notification.connect('clicked', lambda: print('mooooo'))
        #notification.set_urgency(Notify.Urgency.CRITICAL)
        notification.show()

        text_window = TextWindow(clipboard_contents)
        text_window.show_all()

    return callback


def main() -> None:
    Notify.init(BACKWARDS_NAME)

    clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
    clipboard.connect("owner-change", callback(clipboard))

    Gtk.main()


if __name__ == "__main__":
    main()
